#pragma once

#include "ValueNode.h"
#include "AbstractSyntaxTreeNode.h"


class ReturnNode : public AbstractSyntaxTreeNode
{
private:
	ValueNode* returnValue;

public:

	ReturnNode(ValueNode* returnValue);
	virtual std::string ToString();
	virtual std::string ToAssembly();
};

