#include "ConditionNode.h"



ConditionNode::ConditionNode(int leftValue, std::string op, int rightValue) : AbstractSyntaxTreeNode("condition")
{
	this->leftValue = leftValue;
	this->op = op;
	this->rightValue = rightValue;
}

std::string ConditionNode::ToString()
{
	return std::to_string(leftValue) + " " + op + " " + std::to_string(rightValue);
}

std::string ConditionNode::ToAssembly()
{
	return "";
}