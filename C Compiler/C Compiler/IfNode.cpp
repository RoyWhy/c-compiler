#include "IfNode.h"
#include "BlockNode.h"

IfNode::IfNode(ConditionNode* condition, BlockNode* body, bool hasElse, BlockNode* elseBody) : AbstractSyntaxTreeNode("if")
{
	this->condition = condition;
	this->body = body;
	this->elseBody = elseBody;
	this->hasElse = hasElse;
}

std::string IfNode::ToString()
{
	std::string str = "";
	if (hasElse)
	{
		str = "\nelse\n" + elseBody->ToString();
	}
	return "if(" + this->condition->ToString() + ")\n" + this->body->ToString() + str;
}

std::string IfNode::ToAssembly()
{
	return "";
}