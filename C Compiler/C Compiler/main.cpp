#include <iostream>
#include "Lexer.h"
#include "ProgramNode.h"

int main() 
{
	std::string program = "int main()\n{\n\tint x = 5;\n\tif(1>2)\n\t{\n\t\tint y = 65;\nreturn 123;\n\t}\n\telse if(4>2)\n{\nreturn 23;\n}\n}";
	std::cout << program << '\n' << std::endl;
	Lexer* lexer = Lexer::getInstance();
	try
	{
		std::vector<Token*>* a = lexer->generateTokens(program, "main file");

		ProgramNode* p = new ProgramNode(*a);
		std::cout << p->ToAssembly();
	}
	catch (std::exception e)
	{
		std::cerr << e.what() << std::endl;
	}
	delete lexer;
}