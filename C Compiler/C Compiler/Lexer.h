#pragma once

#include <iostream>
#include "Token.h"
#include <vector>



class Lexer
{
private:
	static Lexer* instance;
	Lexer();
	std::string trimSpaces(std::string input);
	const std::string validTokens = "(){};<>[]=";
	std::vector<std::string>* split(std::vector<std::string> *stringList, char divider, bool includeDivider);
public:
	~Lexer();
	static Lexer* getInstance();
	
	std::vector<Token*>* generateTokens(std::string input, std::string filename);
	void deleteTokens(std::vector<Token*>* tokens);

};

