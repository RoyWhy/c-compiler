#include "BlockNode.h"


BlockNode::BlockNode(std::vector<Token*> tokens) : AbstractSyntaxTreeNode("block")
{
	int index = 0;
	for (auto i = tokens.begin(); i != tokens.end(); i++)
	{
		if ((*i)->getType() == "return")
		{
			int progress = HandleReturnCreation(tokens, index);
			i += progress;
			index += progress;
		}
		else if ((*i)->getType() == "if")
		{
			int progress = HandleIfCreation(tokens, index);
			i += progress;
			index += progress;
		}
		else if ((*i)->getType() == "int")
		{
			int progress = HandleVarCreation(tokens, index);
			i += progress;
			index += progress;
		}
		else if ((*(i))->getType() == "for")
		{
			int progress = HandleForLoopCreation(tokens, index);
			i += progress;
			index += progress;
		}
		else if ((*(i))->getType() == "while")
		{
			int progress = HandleWhileLoopCreation(tokens, index);
			i += progress;
			index += progress;
		}
		else if ((*(i + 1))->getType() == "(")
		{
			int progress = HandleFunctionCallCreation(tokens, index);
			i += progress;
			index += progress;
		}
		index++;
	}
}


std::string BlockNode::ToString()
{
	std::string string = "{\n";
	for (AbstractSyntaxTreeNode* node : nodes)
	{
		string += node->ToString() + "\n";
	}
	string += "}";
	return string;
}

std::string BlockNode::ToAssembly()
{
	return "";
}

int BlockNode::findClosingBrackets(std::vector<Token*> tokens, int index)
{
	if (tokens[index]->getType().length() > 1)
	{
		//throw exception
	}
	std::string closeBrackets;
	int bracketsCount = 0;
	switch (tokens[index]->getType()[0])
	{
	case '(':
		closeBrackets = ')';
		break;
	case '{':
		closeBrackets = '}';
		break;
	case '<':
		closeBrackets = '>';
		break;
	case '[':
		closeBrackets = ']';
		break;
	default:
		//throw exception
		break;
	}
	
	for (int i = index; i < tokens.size(); i++)
	{
		if (tokens[i]->getType() == tokens[index]->getType())
		{
			bracketsCount += 1;
		}
		else if (tokens[i]->getType() == closeBrackets)
		{
			bracketsCount -= 1;
		}

		if (bracketsCount == 0)
		{
			return i;
		}
	}

	//throw exception/
	return -1;

}






int BlockNode::HandleVarCreation(std::vector<Token*> tokens, int startIndex)
{
	int index = startIndex;
	auto i = tokens.begin() + index;

	bool isAssigned = (*(i + 2))->getType() == "=";
	int value = 0;
	std::string name = (*(i + 1))->getType();
	i++;
	index++;
	if (isAssigned)
	{
		value = std::atoi((*(i + 2))->getType().c_str());
		i += 2;
		index += 2;
	}

	varNames.push_back(name);
	VarDeclaration* varDeclaration = new VarDeclaration("int", name, isAssigned, value);
	nodes.push_back(varDeclaration);
	return index - startIndex;
}

int BlockNode::HandleIfCreation(std::vector<Token*> tokens, int startIndex)
{
	int index = startIndex;
	auto i = tokens.begin() + index;

	BlockNode* elseBlock = nullptr;
	int elseEndBracket = 0;
	bool hasElse = false;
	int left = std::stoi((*(i + 2))->getType());
	std::string op = (*(i + 3))->getType();
	int right = std::stoi((*(i + 4))->getType());
	ConditionNode* condition = new ConditionNode(left, op, right);
	int ifEndBracket = findClosingBrackets(tokens, index + 6);
	BlockNode* ifBody = new BlockNode(std::vector<Token*>(i + 6, i - index + ifEndBracket));

	if (ifEndBracket + 1 < tokens.size() && (*(i - index + ifEndBracket + 1))->getType() == "else")
	{
		hasElse = true;
		if ((*(i - index + ifEndBracket + 2))->getType() == "if")
		{
			elseEndBracket = findClosingBrackets(tokens, ifEndBracket + 8);
			elseBlock = new BlockNode(std::vector<Token*>(i - index + ifEndBracket + 2, i - index + elseEndBracket + 1));
		}
		else
		{
			elseEndBracket = findClosingBrackets(tokens, ifEndBracket + 2);
			elseBlock = new BlockNode(std::vector<Token*>(i - index + ifEndBracket + 2, i - index + elseEndBracket));
		}
	}

	nodes.push_back(new IfNode(condition, ifBody, hasElse, elseBlock));
	if (hasElse)
	{
		i += elseEndBracket - index;
		index = elseEndBracket;
	}
	else
	{
		i += findClosingBrackets(tokens, index + 6) - index;
		index = findClosingBrackets(tokens, index + 6);
	}

	return index - startIndex;
}

int BlockNode::HandleReturnCreation(std::vector<Token*> tokens, int startIndex)
{
	int index = startIndex;
	auto i = tokens.begin() + index;

	ValueNode* value = new ValueNode(std::atoi((*(i + 1))->getType().c_str()));
	nodes.push_back(new ReturnNode(value));
	if ((*(i + 2))->getType() != ";")
	{
		//raise exception.
	}
	i += 2;
	index += 2;

	return index - startIndex;
}

int BlockNode::HandleForLoopCreation(std::vector<Token*> tokens, int startIndex)
{
	int index = startIndex;
	auto i = tokens.begin() + index;







}

int BlockNode::HandleWhileLoopCreation(std::vector<Token*> tokens, int startIndex)
{
	int index = startIndex;
	auto i = tokens.begin() + index;

	int left = std::stoi((*(i + 2))->getType());
	std::string op = (*(i + 3))->getType();
	int right = std::stoi((*(i + 4))->getType());
	ConditionNode* condition = new ConditionNode(left, op, right);
	int whileEndBracket = findClosingBrackets(tokens, index + 6);
	BlockNode* whileBody = new BlockNode(std::vector<Token*>(i + 6, i - index + whileEndBracket));

	nodes.push_back();

	i += whileEndBracket - index;
	index += whileEndBracket - index;


	return index - startIndex;

}

int BlockNode::HandleFunctionCallCreation(std::vector<Token*> tokens, int startIndex)
{
	int index = startIndex;
	auto i = tokens.begin() + index;

	std::string name = (*i)->getType();
	int closing = findClosingBrackets(tokens, index + 1);

	if ((*(i + closing + 1))->getType() != ";")
	{
		//raise exception.
	}

	std::vector<std::string> args;


	for (int j = index + 2; j < closing; j++)
	{
		if ((*(i - index + j))->getType() != ",")
		{
			args.push_back((*(i - index + j))->getType());
		}
	}

	nodes.push_back(new FunctionCallNode(name, args));

	i += closing - index + 1;
	index += closing - index + 1;
}

