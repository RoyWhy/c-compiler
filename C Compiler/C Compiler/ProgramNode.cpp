#include "ProgramNode.h"

ProgramNode::ProgramNode(std::vector<Token*> tokens) : AbstractSyntaxTreeNode("program")
{
	BlockNode* mainBody = new BlockNode(std::vector<Token*>(tokens.begin() + 4, tokens.begin() + (BlockNode::findClosingBrackets(tokens, 4))));

	this->main = new FunctionNode(tokens[0], tokens[1], mainBody);
}

std::string ProgramNode::ToString()
{
	return this->main->ToString();
}

std::string ProgramNode::ToAssembly()
{
	return this->main->ToAssembly();
}