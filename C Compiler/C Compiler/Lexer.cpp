#include "Lexer.h"

Lexer* Lexer::instance;

Lexer::Lexer()
{
}

Lexer* Lexer::getInstance()
{
	if (Lexer::instance == nullptr)
	{
		Lexer::instance = new Lexer();
	}
	return Lexer::instance;
}

Lexer::~Lexer()
{
}

std::string Lexer::trimSpaces(std::string input)
{
	int trimBeginning = input.find_first_not_of(" \n\r\t");
	int trimEnd = input.find_last_not_of(" \n\r\t");
	input = input.substr(trimBeginning, input.size() - trimBeginning - (input.size() - trimEnd) + 1);

	int spacesInRow = 0;

	for (auto i = input.begin(); i != input.end(); i++)
	{
		if (*i == ' ' || *i == '\n' || *i == '\r' || *i == '\t')
		{
			spacesInRow++;
		}
		else
		{
			spacesInRow = 0;
		}
		if (spacesInRow > 1)
		{
			input.erase(i);
			spacesInRow = 0;
			i = i - 2;
		}
	}


	return input;
}

std::vector<std::string>* Lexer::split(std::vector<std::string>* stringList, char divider, bool includeDivider)
{
	std::vector<std::string>* result = new std::vector<std::string>();
	int index = 0;
	for (auto str : *stringList)
	{
		index = 0;
		while (str.find(divider, index) != std::string::npos)
		{
			if (str.find(divider, index) != index)
			{
				result->push_back(str.substr(index, str.find(divider, index) - index));
			}
			if (includeDivider)
			{
				result->push_back(std::string(1, divider));
			}


			index = str.find(divider, index) + 1;

		}
		if (index < str.size())
		{
			result->push_back(str.substr(index, str.size() - index));
		}
	}
	return result;
}


std::vector<Token*>* Lexer::generateTokens(std::string input, std::string filename)
{
	std::vector<Token*>* tokens = new std::vector<Token*>();
	std::vector<std::string>* stringList = new std::vector<std::string>();
	
	
	input = trimSpaces(input);
	stringList->push_back(input);
	stringList = split(stringList, ' ', false);
	stringList = split(stringList, '\n', false);
	stringList = split(stringList, '\t', false);
	stringList = split(stringList, '\r', false);

	for (auto token : validTokens)
	{
		stringList = split(stringList, token, true);
	}

	for (auto i : *stringList)
	{
		tokens->push_back(new Token(i, filename, 0, 0));
	}
	
	return tokens;
}

void Lexer::deleteTokens(std::vector<Token*>* tokens)
{
	for (auto i : *tokens)
	{
		std::cout << i->getType();
		delete i;
	}
	delete tokens;
}