#include "ReturnNode.h"

ReturnNode::ReturnNode(ValueNode* returnValue) : AbstractSyntaxTreeNode("return")
{
	this->returnValue = returnValue;
}


std::string ReturnNode::ToString()
{
	return "return " + returnValue->ToString() + ";";
}

std::string ReturnNode::ToAssembly()
{
	return "";
}