#include "ValueNode.h"


ValueNode::ValueNode(int value) : AbstractSyntaxTreeNode("ValueNode")
{
	this->value = value;
}


std::string ValueNode::ToString()
{
	return "" + std::to_string(value);
}

std::string ValueNode::ToAssembly()
{
	return "";
}