#pragma once

#include <iostream>
#include <exception>


class Token
{
	
private:
	std::string type;
	std::string fileName;
	int lineNumber;
	int columnCount;
public:
	Token(std::string type, std::string filename, int lineNumber, int columnNumber);

	std::string getType();
};

