#pragma once

#include <iostream>
#include "AbstractSyntaxTreeNode.h"
#include <string>

class ConditionNode : public AbstractSyntaxTreeNode
{
private:
	int leftValue;
	std::string op;
	int rightValue;
public:

	ConditionNode(int leftValue, std::string op, int rightValue);

	virtual std::string ToString();
	virtual std::string ToAssembly();
};

