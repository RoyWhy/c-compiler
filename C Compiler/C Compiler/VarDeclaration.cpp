#include "VarDeclaration.h"
#include <string>

VarDeclaration::VarDeclaration(std::string type, std::string name, bool isAssigned, int value) : AbstractSyntaxTreeNode("variable declaration")
{
	this->type = type;
	this->name = name;
	this->isAssigned = isAssigned;
	this->value = value;
}

std::string VarDeclaration::ToString()
{
	return type + " " + name + (isAssigned ? (" = " + std::to_string(value)) : "") + ";";
}

std::string VarDeclaration::ToAssembly()
{
	return "";
}
