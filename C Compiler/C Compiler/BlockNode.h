#pragma once
#include "AbstractSyntaxTreeNode.h"
#include "Token.h"
#include "ReturnNode.h"
#include "ValueNode.h"
#include "ConditionNode.h"
#include <string>
#include "IfNode.h"
#include "VarDeclaration.h"
#include "FunctionCallNode.h"


class BlockNode : public AbstractSyntaxTreeNode
{
private:
	std::vector<AbstractSyntaxTreeNode*> nodes;

	std::vector<std::string> varNames;

public:
	BlockNode(std::vector<Token*> tokens);
	virtual std::string ToString();
	virtual std::string ToAssembly();
	
	int HandleVarCreation(std::vector<Token*> tokens, int startIndex);
	int HandleIfCreation(std::vector<Token*> tokens, int startIndex);
	int HandleReturnCreation(std::vector<Token*> tokens, int startIndex);
	int HandleFunctionCallCreation(std::vector<Token*> tokens, int startIndex);
	int HandleForLoopCreation(std::vector<Token*> tokens, int startIndex);
	int HandleWhileLoopCreation(std::vector<Token*> tokens, int startIndex);


	static int findClosingBrackets(std::vector<Token*> tokens, int index);
};

