#pragma once

#include <iostream>
#include <vector>

class AbstractSyntaxTreeNode
{
protected:
	std::string type;

	AbstractSyntaxTreeNode(std::string type);
private:
public:
	virtual std::string ToString() = 0;

	virtual std::string ToAssembly() = 0;
};
