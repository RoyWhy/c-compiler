#pragma once
#include <iostream>
#include "AbstractSyntaxTreeNode.h"
#include "BlockNode.h"
#include "Token.h"

class FunctionNode : AbstractSyntaxTreeNode
{
private:
	std::string returnType;
	std::string name;
	BlockNode* body;

public:
	FunctionNode(Token* returnType, Token* name, BlockNode* body);
	virtual std::string ToString();
	virtual std::string ToAssembly();
};

