#pragma once

#include "AbstractSyntaxTreeNode.h"
#include <string>

class ValueNode : public AbstractSyntaxTreeNode
{
	int value;
public:
	ValueNode(int Value);
	virtual std::string ToString();
	virtual std::string ToAssembly();
};

