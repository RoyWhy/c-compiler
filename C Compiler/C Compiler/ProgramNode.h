#pragma once

#include "AbstractSyntaxTreeNode.h"
#include "FunctionNode.h"
#include "Token.h"
#include "BlockNode.h"

class ProgramNode : public AbstractSyntaxTreeNode
{
private:
	FunctionNode* main;
public:
	ProgramNode(std::vector<Token*> tokens);
	virtual std::string ToString();
	virtual std::string ToAssembly();
};

