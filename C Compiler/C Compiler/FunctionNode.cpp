#include "FunctionNode.h"

FunctionNode::FunctionNode(Token* returnType, Token* name, BlockNode* body) : AbstractSyntaxTreeNode("function")
{
	this->returnType = returnType->getType();
	this->name = name->getType();
	this->body = body;
}


std::string FunctionNode::ToString()
{
	return returnType + " " + name + "()\n" + body->ToString();
}

std::string FunctionNode::ToAssembly()
{
	return ".global _start\n.text\n_start:\nmov $60, %rax\nmov $0, %rdi\nsyscall";
}