#pragma once

#include "ConditionNode.h"
#include "AbstractSyntaxTreeNode.h"

class BlockNode;

class IfNode : public AbstractSyntaxTreeNode
{
private:

	BlockNode* body;
	ConditionNode* condition;
	BlockNode* elseBody;
	bool hasElse;

public:

	IfNode(ConditionNode* condition, BlockNode* body, bool hasElse, BlockNode* elseBody);

	virtual std::string ToString();
	virtual std::string ToAssembly();
};

