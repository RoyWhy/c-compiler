#pragma once

#include "AbstractSyntaxTreeNode.h"


class FunctionCallNode : public AbstractSyntaxTreeNode
{
private:
	std::string name;
	std::vector<std::string> args;


public:
	FunctionCallNode(std::string name, std::vector<std::string> args) : AbstractSyntaxTreeNode("function call")
	{
		this->args = args;
		this->name = name;
	}

	virtual std::string ToString()
	{
		return "";
	}
	virtual std::string ToAssembly()
	{
		return "";
	}


};

