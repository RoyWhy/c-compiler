#pragma once

#include <iostream>
#include "AbstractSyntaxTreeNode.h"

class VarDeclaration : public AbstractSyntaxTreeNode
{
private:
	std::string type;
	std::string name;
	bool isAssigned;
	int value;

public:
	VarDeclaration(std::string type, std::string name, bool isAssigned, int value);

	virtual std::string ToString();
	virtual std::string ToAssembly();

};

